# monitoring

# Installation
Install docker and init swarm
```sh
docker swarm init
```

```sh
cd /srv
git clone https://gitlab.com/YaZasnyal-docker/monitoring.git
cd monitoring
docker stack deploy -c docker-compose.yml monitoring
```

Open grafana in your browser.
